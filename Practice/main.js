console.log("helow")
console.log("Test")

// Create variable 

// var variableName = "Welcome to varibles";

//ES6 variables  Name  
let variableName = "Welcome to varibles"; // --> recomended 
const name = "asdsad" // --> recomended
// Old verion varible 
var Name = "asdsad"// not recomended
console.log(variableName);



// VARIBLE NAMING  
//1.The name should be unique 
// 2.The name should be not be  reserve keyword 
// 3.The name must be start with character, _



// Types of Data Types 
// 1. String 
// double and single is no difference  is only identical 
// backtic --> is a complecx we can code or wrap or embed with ${} variable it is conveniece knows as template literals 
const FullName = "Bong";
const exampleStringDoubleQoute = "Hello word";
const exampleStringSingleQoute = 'Hello word';
const bactics = `hie, ${FullName}, ${2 + 2}`;
console.log(typeof exampleStringDoubleQoute);
console.log(exampleStringSingleQoute);
console.log(bactics);


//2. Number 

const wholeNumber = 5; // --> whole number
const decimalNumber = 1.5; // float/decimal number
console.log(wholeNumber);
console.log(decimalNumber);


const firstNumber = 5;
const SecondNumber = 5;

const string = 'helloe';

const result = string / SecondNumber; // -> is NaN or not a number
console.log(result);
console.log(typeof result);

// Boleans
// true - yes, correct, --> value is 1  
// true - no, incorrect, --> value is 0 

const isCool = true;
const inNotCool = false;
if (inNotCool) {
    console.log("It is true");
} else {
    console.log("It is false");
}

const age = 20;
console.log(age > 17);


// 3.Null or nothing
null

const nullExample = null;
console.log(nullExample); // or nothing
console.log(typeof nullExample); // or nothing

//4. undifined or no value 

let x = undefined;
console.log(x);
console.log(typeof x);


//5.Object the most important datatype in comes to building block data
// Object collection of data 
// Object is the complex data type
const person = {
    // propertie with thier value 
    // key-value properties
    name: "Vhong",
    age: "23"
}
// to access the certain properties use Dot notation
console.log(typeof person);
console.log(person.name);
console.log(typeof person.name);
console.log(person.age);


// Array 

const arr = [1, 2, 3, 4, 5];
console.log(arr);

const date = new Date();
console.log(date);
console.log(typeof date);



// Dynamically typed
// javascrpt is dynamic language which accect vareity of data types and can reasign new value  Ex. string able to put another value Ex number

//MODULO OPERATOR getting the ramainder left overpand divided

const g = 10;
const h = 5;
console.log(g % h);

// ==== COMPARISON OPERATOR ==== true/false//


const a = 5;
const b = 10;
// Greter then
console.log(a > b);


// less Than o
console.log(a < b)

// greater Than or equal to 
console.log(a >= b);

// less than or equal to 
console.log(a <= b);



// Is equal sign 

const s = 10;
const p = 10;

console.log(s == p);


// not eqaul 
const q = 10;
const z = 10
console.log(q != z)


// Srict eqaullty 
console.log(q === z);


const Age = 18;

if (Age > 89) {
    console.log("you may Enter")

} else if (Age === 89) {
    console.log("you Just turned 18")
} else {
    console.log("Grow up")
}

// lOGICAL Operator
// AND && --> ALL OPERANDS ARE TRUE --> true
// AND && it must all condition its true value -->
// Ex example console.log(true && true && true);

// OR || --> AT LEADST ONE OPERAND IS TRUE --> true -
// OR || it must be at least have one true in the condition
// conosole.log(true || true || false);

// NOT --> must be have false in the condition
//Ex console.log(!false)



// the While loop 
let i = 0

while (i <= 10) {
    // known as iteration 
    console.log(i) // don't do that it is infinit may be log you bwroser engine
    i++;
}


for (let i = 0; i < 10; i++) {
    const element = i;
    console.log(element);

}



// ===== FUNCTIONS ==== //
// Function is the most important a block of code to perform the task

// A FUNCTION DECLARATION (defining a function)
function number(number) {
    console.log("I am Here")
    return number * number;
}

// A FUNCTION CALL (callling/executing/ invoking a function )

console.log(number(5));


// A function Declarations 

// function name(params) {
//     // statements
//     // hava access to "this" keyword
// }

// // A function Expression 

// const name = function (params) {
//     //statements
// }


// // An arrow function Expression

// const name = (params) => {
//     // statements
// }



function sayHi(name) {
    console.log(`Hi, ${name}`);
}

sayHi("vhong");


function add(a, b) {
    return a + b;
}

const sum = add(1, 3);
console.log(sum);



// Arrow function 

const Numbermulti = (number) => {
    return number * number;
}


const resultMultiply = Numbermulti(5);
console.log(resultMultiply);


const square = (number) => number * number;
const resultTimes = square(8);
console.log(resultTimes);


const loopingMultiply = (number) => {
    for (let i = 0; i <= number; i++) {
        const loopNumber = i;
        const times = loopNumber * 5;
        const multiplybyfive = `multiplication ${loopNumber} * 5 = ${times}`;
        console.log(multiplybyfive);
    }

}


loopingMultiply(100); 




const promise1 = new Promise((resolve, reject) => )