const nextEl = document.getElementById("next");
const stepsEl = document.querySelectorAll(".step");
const prevEL = document.getElementById("prev");
const progressEL = document.querySelector(".progress-bar-front");



let currentCheked = 1;

nextEl.addEventListener("click", () => {
    currentCheked++;


    if (currentCheked > stepsEl.length) {
        currentCheked = stepsEl.length;

    }
    updateStepProgress()
    // console.log(currentCheked);
});


prevEL.addEventListener("click", () => {
    currentCheked--;


    if (currentCheked < 1) {
        currentCheked = 1;

    }
    updateStepProgress()
    // console.log(currentCheked);
});


function updateStepProgress() {
    stepsEl.forEach((step, index) => {
        if (index < currentCheked) {
            step.classList.add("check");
            step.innerHTML = `<i class="fa-solid fa-check"></i> <small>${index === 0 ? "Start" : index === stepsEl.length - 1 ? "Final" : "Step" + index}</small>`;
        } else {
            step.classList.remove("check");
            step.innerHTML = `<i class="fas fa-times"></i>`
        }
    });

    const checkNumber = document.querySelectorAll(".check");
    progressEL.style.width = ((checkNumber.length - 1) / (stepsEl.length - 1)) * 100 + "%";

    if (currentCheked === 1) {
        prevEL.disabled = true;
    }
    else if (currentCheked === stepsEl.length) {
        nextEl.disabled = true;
    } else {
        prevEL.disabled = false;
        nextEl.disabled = false;

    }

}
