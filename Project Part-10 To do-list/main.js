const formEl = document.querySelector(".form");
// console.log(formEl);
const inputEl = document.querySelector(".input");
const ulEL = document.querySelector(".list");

//We get the data from WEB API stored which is the list from the key in the local storage
// We create the data unto the local was convert in JSON stringify 
let list = JSON.parse(localStorage.getItem("list"));
console.log(list);

list.forEach(task => {
    toDolist(task);
})
formEl.addEventListener("submit", (event) => {
    event.preventDefault();
    toDolist();
})

function toDolist(task) {
    let newTask = inputEl.value;
    if (task) {
        newTask = task.name
    }
    const liEl = document.createElement("li");
    if (task && task.checked) {
        liEl.classList.add("checked");
    }
    liEl.innerText = newTask;
    ulEL.appendChild(liEl);
    // remeove the value 
    inputEl.value = "";
    // append the icon check along the li <element> a</element>
    const checkBtnEL = document.createElement("div");
    checkBtnEL.innerHTML = `<i class="fa-solid fa-square-check">`;
    liEl.appendChild(checkBtnEL);
    // append the icon transh along the li element
    const trashBtnEL = document.createElement("div");
    trashBtnEL.innerHTML = `<i class="fa-solid fa-trash"></i>`;
    liEl.appendChild(trashBtnEL);

    checkBtnEL.addEventListener("click", () => {
        liEl.classList.toggle("checked");
        udateLocalStorage()
    })

    trashBtnEL.addEventListener("click", () => {
        liEl.remove();
        udateLocalStorage()
    });

    udateLocalStorage()
}

// We create data objects that converted in JSON stringify function which is li element 
// we loop every li since thee have function
function udateLocalStorage() {
    const liEls = document.querySelectorAll("li");
    list = [];
    liEls.forEach(liEl => {
        list.push({
            name: liEl.innerText,
            checked: liEl.classList.contains("checked") // condition if check is trur if doest not will  be false in the local local storage
        });
    })

    // convert unto stringify 
    localStorage.setItem("list", JSON.stringify(list));
}       