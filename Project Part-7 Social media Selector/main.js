const menuEL = document.querySelector(".menu");
const menuTextEl = document.querySelector(".menu p");
const socialListEl = document.querySelector(".social-lists");
const listEL = document.querySelectorAll(".social-lists li");
// // const arrowIconEl = document.querySelector(".menu .fa-solid")
// console.log(listEL);
menuEL.addEventListener("click", () => {
    socialListEl.classList.toggle("hide");
    menuEL.classList.toggle("rotate");

})

listEL.forEach(list => {
    list.addEventListener("click", () => {
        menuTextEl.innerHTML = list.innerHTML;
        socialListEl.classList.toggle("hide");
        menuEL.classList.remove("rotate");
        menuEL.classList.toogle("rotate");
    })
})