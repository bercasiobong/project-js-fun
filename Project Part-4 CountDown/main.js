const dayEL = document.getElementById("day");
const hourEL = document.getElementById("hour");
const minuteEL = document.getElementById("minute");
const secondEL = document.getElementById("second");


const newYearTime = new Date("Jan 1,2023 00:00:00").getTime();


updateCountDown();


function updateCountDown() {
    const now = new Date().getTime()
    const gap = newYearTime - now;
    console.log(gap)



    const second = 1000;
    const minute = second * 60;
    const hour = minute * 60;
    const day = hour * 24;
    console.log(day)





    const d = Math.floor(gap / day);
    const h = Math.floor((gap % day) / hour);
    const m = Math.floor((gap % hour) / minute);
    const s = Math.floor((gap % minute) / second);


    dayEL.innerText = d;
    hourEL.innerText = h;
    minuteEL.innerText = m;
    secondEL.innerText = s;


    // setTimeout(() => {
    //     updateCountDown()
    // }, 1000);

    // or 
    // short hand
    setTimeout(updateCountDown, 1000)
}