const btnEl = document.querySelector(".btn");
const iputEL = document.getElementById("input");
const copyIconEl = document.querySelector(".fa-copy");
const alertContainerEl = document.querySelector(".alert-container");

btnEl.addEventListener("click", () => {
    creatPassword();
})

copyIconEl.addEventListener("click", () => {
    copyPassword();
})

function creatPassword() {
    const chars = "1234567890abcdefghijklmopqrstuvwxyz!@#$%^&*()_+?:{}[]ABCDEFGHJKLMOPQRSTUVWXYZ";

    //  looping the random
    const passwordsLength = 14;
    let password = "";
    for (let index = 0; index < passwordsLength; index++) {
        const randomNum = Math.floor(Math.random() * chars.length);
        password += chars.substring(randomNum, randomNum + 1);
    }
    // getting after the random
    iputEL.value = password;
    // copy the password
    alertContainerEl.innerText = password + "Copied!"
}

// Copy password from random 

function copyPassword() {
    // used the build in select method
    iputEL.select();
    // .setSelectionRange method is for mobile purposes if you want to see it
    iputEL.setSelectionRange(0, 999);
    navigator.clipboard.writeText(iputEL.value);
    //notify the copying
 
    if (iputEL.value) {
        alertContainerEl.classList.remove("active");
        setTimeout(() => {
            alertContainerEl.classList.add("active");
        }, 2000);
    }


}